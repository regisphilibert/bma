//@codekit-prepend "libs/owl.carousel.min.js"
//@codekit-prepend "libs/jquery.waypoints.min.js"
//@codekit-prepend "libs/waypoints.inview.min.js"
//@codekit-prepend "libs/jquery.uniform.min.js"
//@codekit-prepend "libs/imagesloaded.min.js"
//@codekit-prepend "libs/imagelightbox.min.js"
//@codekit-prepend "libs/isotope.min.js"
animSpeed = 250;

tablet = 760;
$(document).ready(function(){
    if($('form').length){
        $('.uniform').uniform(
            {
                fileButtonHtml: 'Fichier&hellip;',
                selectAutoWidth: false,
                fileDefaultHtml: 'Aucun fichier choisi'
            }
        );
        if($('.career-form').length){
            $('.career-form').append("<input type='hidden' name='annonce-initiale' value='"+$('.item-head .item-title').html()+"'/>");
        }
    }
    if($('.bma-slider').length){
        imagesLoaded( '.bma-slider', function() {
            var inview = new Waypoint.Inview({
                element: $('.home .bma-slider')[0],
                enter: function(direction) {
                    if(direction == 'up'){
                        $('.bma-header').removeClass('default').addClass('special');
                    }
                },
                exited: function(direction) {
                    if(direction == 'down'){
                        $('.bma-header').removeClass('special').addClass('default');
                    }
                }
            });
        });
    }
    $('.bma-header .js-display-nav').click(function(){
        $(this).toggleClass('active');
        $('.bma-header').addClass('is-animating');
        if($(window).width() < tablet){
            $('.bma-main-nav').slideToggle(animSpeed, function(){
                $('.bma-header').removeClass('is-animating');
                $('.bma-header').removeClass('is-showing');
            });
        }
        else{
            $('.bma-header').toggleClass('is-showing');
            setTimeout(function(){
                $('.bma-header').removeClass('is-animating');
            }, animSpeed);
        }
    });
    $('.js-show-desc').click(function(){
        if($(window).width() < 960){
            $(this).toggleClass('showing');
            $(this).siblings('.js-show').slideToggle(250);
        }
    });
    var owlSlider = $('.bma-slider');
    owlSlider.owlCarousel({
        loop:true,
        dotsSpeed : 400,
        items:1,
        lazyLoad:true,
        navRewind:false,
    });
    $(".bma-slider-container .slider-next").click(function(){
        owlSlider.trigger('next.owl.carousel');
    });
    $(".bma-slider-container .slider-prev").click(function(){
        owlSlider.trigger('prev.owl.carousel');
    });

    if($('.bma-column-sort').length){
        directorySort($('.bma-column-sort'), 2, $('.bma-column-sort').fadeTo(150,1));
    }
    if($('[data-align-with]').length){
        alignWith($('.item-content h2:first-of-type'));
    }
    if($('.bma-gallery').length){
        imagesLoaded( '.bma-gallery', function() {
            var instanceLB = $('.bma-gallery a').imageLightbox({
                onStart:function(){overlayOn(); closeButtonOn(instanceLB); arrowsOn(instanceLB, '.bma-gallery a');},
                onEnd:function(){overlayOff(); captionOff(); closeButtonOff(); arrowsOff();},
                onLoadStart:function() { captionOff(); activityIndicatorOn(); },
                onLoadEnd:function() { captionOn(); activityIndicatorOff(); $( '.imagelightbox-arrow' ).css( 'display', 'block' ); }
            });
        });
    }

   // init Isotope
    imagesLoaded( '.bma-loop.isotope-go', function() {
        $container = $('.bma-loop.isotope-go .row').isotope({});
        if($('body[data-realization-type]').length){
            $container.isotope({ filter: "."+$('body').attr('data-realization-type') });
        }
    });
    // filter items on button click
    $('.history .realization-archive .bma-filters').on( 'click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        var typeValue  = $(this).attr('data-type');
        $container.isotope({ filter: filterValue });
        $(this).addClass('active').siblings('a').removeClass('active');
        thisUrl = filterValue != '*' ? '/realisations/'+typeValue : '/realisations';
        if(window.history && window.history.pushState){
            window.history.pushState({filter:filterValue},"", thisUrl);
            if(typeof _gaq !== 'undefined'){
                _gaq.push(['_trackPageview', thisUrl]);
            }
        }
        return false;
    });
    if(Modernizr.history){
        window.addEventListener("popstate", function(event) {
            $container.isotope({ filter: event.state.filter });
        });
    }

    /*********************************************
                MORE POSTS
    *********************************************/
    ajaxOffset = $('.bma-loop').attr('data-limit');
    $(document).on("click", "#ajax_more.ready", function(){
        $(this).addClass('animationHeartBeatText');
        that = $(this);
        $.post(ajaxurl, "offset="+ajaxOffset+"&action=more_posts_action", function(response){
            if(response != '0'){
                $('.bma-loop .row').append(response);
                var delay = 0;
                $('.bma-loop .row .ajax-pending').each(function(){
                    $(this).delay(delay).animate({
                        opacity:1
                    },50);
                    delay += 50;
                });
                ajaxOffset += 6;
                console.log("count : "+$('.bma-loop').attr('data-count-posts'));
                console.log("articles : "+$('.bma-main').find('article').length);
                console.log("---------------------");
                if($('.bma-loop').attr('data-count-posts') == $('.bma-main').find('article').length){
                    $('.bma-more-nav .bma-btn').addClass('bma-disabled').removeClass('ready');
                    console.log("stop");
                }
            }
            else{
                that.fadeTo(250, 0, function(){
                    that.html(that.attr('data-alt-message')).fadeTo(250,1);
                });
            }
            that.removeClass('animationHeartBeatText');
        });
        return false;
    });
    /*********************************************
                BUILDING MAPS
    *********************************************/
    if($('.bma-address-map').length){
        imagesLoaded( '.bma-address-map', function() {
            $('.bma-address-map').each(function(){
                target = $(this).find('div');
                bmaBuildMap(target.attr('id'), target.attr('data-lat'), target.attr('data-long'));
            });
        });
    }
});

function alignWith(element){
    var parent = element.parent();
    $('<div class="row"/>').appendTo(parent);
    $('<div class="col-md-7 bma-aligned-left"/>').appendTo('body');
    $('<div class="col-md-5 bma-aligned-right"/>').appendTo('body');
    //element.nextAll().appendTo(parent.find('.first'));
    element.add(element.nextAll()).appendTo($('.bma-aligned-left'));
    $('.bma-aligned-right').append($('[data-align-with]'));
    $('.bma-aligned-left, .bma-aligned-right').wrapAll('<div class="row bma-aligned"/>');
    $(parent).append($('.row.bma-aligned'));
}

function directorySort(directory, columns, callback){
    if(typeof columns == 'undefined'){
        columns = 4;
    }
    directory.addClass('row');
    $('<div class="col-md-6 first"/>').appendTo(directory);
    $('<div class="col-md-6 second"/>').appendTo(directory);
    full_count = directory.find('.list-item').length;
    i=1;
    directory.find('.bma-group-list').each(function(){
        if(i <= 2){
            $('.first').append($(this));
        }
        else{
            $('.second').append($(this));
        }
        i++;
    });
    if(typeof callback == 'function' ){
        return callback;
    }
}

/*********************************************
            MAPS
*********************************************/
markers = {};
function bmaBuildMap(id, lat, long){
    var styles = [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}];
    var myLatlng = new google.maps.LatLng(lat, long);
    var mapOptions = {
      zoom: 13,
      zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
        },
      scrollwheel: false,
      scaleControl: false,
      styles :  styles,
      disableDefaultUI: true,
      center:myLatlng
    };
    var map = new google.maps.Map(document.getElementById(id), mapOptions);
    image = "https://mapbuildr.com/assets/img/markers/solid-pin-orange.png";
    var marker = new google.maps.Marker({
      position: mapOptions.center,
      map: map,
      icon:image,
      title:document.getElementById(id).getAttribute('data-title')
    });
    var infoWindow = new google.maps.InfoWindow({
        content: "<strong>Bourassa Maillé architectes</strong><br>" + document.getElementById(id).getAttribute('data-title') + "<br><a href='"+$("#"+id).next('a').attr('href')+"' target='_blank'>Voir sur Google Map</a>"
    });
    google.maps.event.addListener(marker, 'click', function() {
        infoWindow.open(map,marker);
    });
    console.log("we build the map");
}
/*********************************************
            FUNCTIONS FOR LIGHTBOX
*********************************************/

// ACTIVITY INDICATOR

var activityIndicatorOn = function()
{
    $( '<div id="imagelightbox-loading"><div></div></div>' ).appendTo( 'body' );
},
activityIndicatorOff = function()
{
    $( '#imagelightbox-loading' ).remove();
},


// OVERLAY

overlayOn = function()
{
    $( '<div id="imagelightbox-overlay"></div>' ).appendTo( 'body' );
},
overlayOff = function()
{
    $( '#imagelightbox-overlay' ).remove();
},


// CLOSE BUTTON

closeButtonOn = function( instance )
{
    $( '<button type="button" id="imagelightbox-close" title="Close">x</button>' ).appendTo( 'body' ).on( 'click touchend', function(){ $( this ).remove(); instance.quitImageLightbox(); return false; });
},
closeButtonOff = function()
{
    $( '#imagelightbox-close' ).remove();
},


// CAPTION
captionIsOn = false;
captionOn = function()
{
    setTimeout(function(){
        var description = $('.bma-gallery').attr('data-title');
        var location = $('.bma-gallery').attr('data-location');
        var thisIndex = $('a[href="' + $( '#imagelightbox' ).attr('src')+'"]').attr( 'data-index' );
        var thisCount = $('.bma-gallery .gallery-item').length;
        lbImage = $('#imagelightbox');
        eTop = lbImage.offset().top;
        capTop = (eTop - $(window).scrollTop()) + eval(lbImage.height());
        $( '<div id="imagelightbox-caption"><div class="caption-left"><div class="gallery-title">' + description + '</div><div class="gallery-location">' + location + '</div></div><div class="caption-right">'+thisIndex+'/'+thisCount+'</div></div>' ).css({
                'width': lbImage.width(),
                'left':lbImage.offset().left,
                'top':capTop,
            }).appendTo( 'body' );
    }, 150);
},
captionOff = function()
{
    $( '#imagelightbox-caption' ).fadeOut(250, function(){$( '#imagelightbox-caption' ).remove();});
    captionIsOn = false;
},


// NAVIGATION

navigationOn = function( instance, selector )
{
    var images = $( selector );
    if( images.length )
    {
        var nav = $( '<div id="imagelightbox-nav"></div>' );
        for( var i = 0; i < images.length; i++ )
            nav.append( '<button type="button"></button>' );

        nav.appendTo( 'body' );
        nav.on( 'click touchend', function(){ return false; });

        var navItems = nav.find( 'button' );
        navItems.on( 'click touchend', function()
        {
            var $this = $( this );
            if( images.eq( $this.index() ).attr( 'href' ) != $( '#imagelightbox' ).attr( 'src' ) )
                instance.switchImageLightbox( $this.index() );

            navItems.removeClass( 'active' );
            navItems.eq( $this.index() ).addClass( 'active' );

            return false;
        })
        .on( 'touchend', function(){ return false; });
    }
},
navigationUpdate = function( selector )
{
    var items = $( '#imagelightbox-nav button' );
    items.removeClass( 'active' );
    items.eq( $( selector ).filter( '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ).index( selector ) ).addClass( 'active' );
},
navigationOff = function()
{
    $( '#imagelightbox-nav' ).remove();
},


// ARROWS

arrowsOn = function( instance, selector )
{
    var $arrows = $( '<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>' );

    $arrows.appendTo( 'body' );

    $arrows.on( 'click touchend', function( e )
    {
        e.preventDefault();

        var $this   = $( this ),
            $target = $( selector + '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ),
            index   = $target.index( selector );

        if( $this.hasClass( 'imagelightbox-arrow-left' ) )
        {
            index = index - 1;
            if( !$( selector ).eq( index ).length )
                index = $( selector ).length;
        }
        else
        {
            index = index + 1;
            if( !$( selector ).eq( index ).length )
                index = 0;
        }

        instance.switchImageLightbox( index );
        return false;
    });
},
arrowsOff = function()
{
    $( '.imagelightbox-arrow' ).remove();
};