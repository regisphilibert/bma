<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php wp_title(''); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
        <?php if(is_single()): ?>
            <?php $og = og_object(); ?>
            <meta property="og:title" content="<?php echo $og->title; ?>" />
            <meta property="og:description" content="<?php echo $og->description; ?>" />
            <meta property="og:image" content="<?php echo $og->image; ?>" />
            <meta property="og:url" content="<?php the_permalink(); ?>" />
            <meta property="og:type" content="website" />
            <meta property="og:site_name" content="<?php bloginfo('name') ?>" />
        <?php else: ?>
            <meta property="og:title" content="<?php bloginfo('name'); ?>" />
            <meta property="og:image" content="<?php echo home_url('/') ?>logo.png" />
            <meta property="og:url" content="<?php echo home_url('/') ?>" />
            <meta property="og:type" content="website" />
            <meta property="og:site_name" content="<?php bloginfo('name') ?>" />
        <?php endif; ?>
        <?php wp_head(); ?>
    </head>