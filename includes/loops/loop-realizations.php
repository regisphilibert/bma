<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
$type = bma_get_the_type();
?>
<!-- post -->
<div class="col-sm-6 col-md-4 sm-no-padding <?php echo $type->slug ?>">
    <a href="<?php the_permalink(); ?>">
        <article <?php post_class('bma-item loop-item') ?>>
            <div class="item-thumb" style="background-image:url(<?php echo bma_get_thumb_uri('bma_480', true); ?>)">
                <img class="tablet-block desktop-block" src="<?php echo get_theme_image('bma-frameholder-46.png'); ?>" alt="">
                <img class="mobile-block" src="<?php echo bma_get_thumb_uri('bma_480', true) ?>" alt="">
            </div>
            <div class="loop-item-content">
                <div class="loop-item-title">
                    <?php bma_the_title(50); ?>
                </div>
                <div class="loop-item-meta">
                    <?php echo bma_the_location(); ?><?php if(is_debug_mode()){ echo ' | '.$type->name;}?>
                </div>
            </div>
        </article>
    </a>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>