<?php if(have_posts()): ?>
<div class="bma-loop-job">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="loop-item-job">
            <a href="<?php the_permalink(); ?>" class="loop-item-title"><?php the_title(); ?></a>
            <div class="loop-item-meta">
                <?php echo get_field('bma_job_location'); ?>
            </div>
        </div>
<?php endwhile; ?>
<!-- post navigation -->
</div>
<?php else: ?>
<div class="bma-loop-job bma-grayed">
    <div class="loop-item-job">
        <div class="loop-item-title"><?php echo _e("Désolé, aucun poste n'est disponible pour le moment", 'bma'); ?></div>
        <div class="loop-item-meta"><?php echo _e("Vous pouvez toutefois soumettre votre candidature.", 'bma') ?></div>
    </div>
</div>
<?php endif; ?>
