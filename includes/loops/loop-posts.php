<?php
global $template_params;

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
<?php  ?>
<div class="<?php echo $template_params['col_class']; ?>">
    <a href="<?php the_permalink(); ?>">
        <article <?php post_class('bma-item loop-item') ?>>
            <div class="item-thumb" style="background-image:url(<?php echo bma_get_thumb_uri('bma_480', true); ?>)">
                <img class="tablet-block desktop-block" src="<?php echo get_theme_image('bma-frameholder-46.png'); ?>" alt="">
                <img class="mobile-block" src="<?php echo bma_get_thumb_uri('bma_crop_90', true) ?>" alt="">
                <div class="item-category item-bottom-right tablet-block desktop-block">
                    <?php bma_the_category(false); ?>
                </div>
                <?php if(is_debug_user()): ?>
                <div class="item-category item-top-left tablet-block desktop-block">
                    <?php echo $post->post_status; ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="stacked-item-thumb-tablet">
                <img src="<?php echo bma_get_thumb_uri('bma_480', true); ?>" alt="<?php the_title(); ?>">
            </div>
            <div class="loop-item-content">
                <div class="loop-item-meta mobile-block">
                    <?php bma_the_category(true); ?>
                </div>
                <div class="loop-item-title">
                   <a href="<?php the_permalink(); ?>" title="<?php bma_the_title(); ?>"><?php bma_the_title(100); ?></a>
                </div>
                <div class="loop-item-meta tablet-block desktop-block">
                    <?php echo _e('Par', 'bma') ?> <?php bma_the_author(true); ?> - <?php bma_the_date(); ?>
                </div>
                <div class="stacked-loop-item-text">
                    - <br>
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </article>
    </a>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>