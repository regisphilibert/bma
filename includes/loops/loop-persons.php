<?php global $persons; ?>
<div class="loop-persons row">
    <?php foreach($persons as $post): ?>
    <div class="loop-person-item col-md-3">
        <div class="person-item-wrapper js-show-desc">
            <div class="person-image-wrapper">
                <div class="slide-up">
                    <div style="padding:15px;">
                        <?php echo get_field('bma_person_excerpt') ?>
                    </div>
                </div>
                <img src="<?php bma_the_person_image(0); ?>" alt="<?php the_title(); ?>" class="person-image tablet-block desktop-block">
            </div>
            <div class="person-item-name"><?php the_title(); ?></div>
            <div class="person-item-title">
                <?php echo get_field('bma_person_title'); ?>
             </div>
        </div>
        <div class="person-item-desc js-show">
            <?php echo get_field('bma_person_excerpt') ?>
        </div>
    </div>
    <?php endforeach; ?>
</div>