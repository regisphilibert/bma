<?php global $persons; ?>
<div class="loop-persons row">
    <?php for($i=0; $i < 25; $i++): ?>
    <div class="loop-person-item col-md-3">
        <div class="person-item-wrapper js-show-desc">
            <div class="person-image-wrapper">
                <div class="slide-up">
                    <div style="padding:15px;">
                        Curabitur blandit tempus porttitor. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus. Nulla vitae elit libero, a pharetra augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </div>
                </div>
                <img src="<?php bma_the_person_image(); ?>" alt="<?php the_title(); ?>" class="person-image tablet-block desktop-block">
            </div>
            <div class="person-item-name"><?php bma_the_author(); ?></div>
            <div class="person-item-title">
                Architecte
             </div>
        </div>
        <div class="person-item-desc js-show">
            Curabitur blandit tempus porttitor. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus. Nulla vitae elit libero, a pharetra augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </div>
    </div>
    <?php endfor; ?>
</div>