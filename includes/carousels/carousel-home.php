<?php global $post; ?>
<style>
    .owl-dot{
        width:<?php echo 100 / count(get_field('bma_carousel_slides')); ?>%
    }
</style>
<div class="bma-slider-container first-slide">
    <div class="slider-next"></div>
    <div class="slider-prev"></div>
    <div class="bma-slider owl-carousel owl-theme">
    <?php while(have_rows('bma_carousel_slides')): the_row();
    $item = current(get_sub_field('bma_carousel_slides_post'));
    $item_type = bma_get_the_type($item->ID);
     ?>
        <div class="bma-slide" style="background-image:url(<?php the_sub_field('bma_carousel_slides_image'); ?>)">
            <img class="owl-mobile-base" src="<?php echo get_theme_image('bma-slider-mobile-frame.gif') ;?>" alt="<?php the_title($item->post_title); ?>">
            <img class="owl-regular-base" src="<?php echo get_theme_image('bma-slider-desktop-frame.gif'); ?>" alt="<?php the_title($item->post_title); ?>">
            <div class="bma-slide-caption">
                <a href="<?php echo site_url()."/realisations/".$item_type->slug."/".$item->post_name; ?>"><strong><?php echo get_the_title( $item->ID ); ?><span class="sep">|</span></strong><span class="add-plus"><?php echo bma_get_location($item->ID); ?></span></a>
            </div>
        </div>
    <?php endwhile; ?>
    </div>
</div>