<?php global $post;
    $social_sentence = __("À lire sur le blogue de Bourassa Maillé architectes: ", "bma");
?>
<nav class="item-socials socials">
   <a target="_blank" href="<?php echo shareFb(get_permalink(), $social_sentence.get_the_title()) ?>" class="facebook"></a>
   <a target="_blank" href="<?php echo shareTw(wp_get_shortlink(), $social_sentence.shortenIt(get_the_title(), '60', '...')) ?>" class="twitter"></a>
   <a target="_blank" href="<?php echo shareLinkedIn(get_permalink(), get_the_title(), get_the_excerpt()) ?>" class="linkedin"></a>
   <a href="<?php echo shareEmail(get_the_title(), 'Bonjour, Je te suggère de lire cet intéressant article sur le blogue de Bourassa Maillé architectes: '.get_permalink()); ?>" class="mail"></a>
</nav>