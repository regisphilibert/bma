<?php get_template_part('includes/head'); ?>
    <body <?php body_class(); ?> <?php bma_body_attr(); ?>>
        <?php get_header(); ?>
        <?php if(has_head_img()){
            get_template_part( 'includes/head-img');
        } ?>
        <div class="<?php echo bma_container_class() ?>" data-no-padding="<?php echo bma_container_type() ?>">
            <div class="bma-main" role="main">