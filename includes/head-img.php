<?php global $post; ?>
<style>
    @media screen and (min-width:480px){
        .item-head-img{
            background-image:url(<?php echo rp_get_attachment_image_src(get_post_thumbnail_id(), 'full');?>);
        }
    }
    @media screen and (min-width:992px){
        <?php if(get_field('bma_realization_head_image')): ?>
        .item-head-img{
            background-image:url(<?php echo get_field_image('bma_realization_head_image');?>)
        }
        <?php endif; ?>
    }
</style>
<div class="item-head-img">
    <?php the_post_thumbnail( 'bma_480' ) ?>
</div>