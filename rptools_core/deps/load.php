<?php

/*********************************************
            ACF 5
*********************************************/

require_once( get_stylesheet_directory() . '/' . ARTOOLS_DIR . '/deps/acf-pro/acf.php' );
require_once( get_stylesheet_directory() . '/' . ARTOOLS_DIR . '/deps/acf-pro/api/api-helpers.php');
add_filter('acf/settings/path', 'my_acf_settings_path');

function my_acf_settings_path( $path ) {

  // update path
  $path = get_stylesheet_directory() . '/' . ARTOOLS_DIR . '/deps/acf-pro/';

  // return
  return $path;

}

add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir( $dir ) {

  // update path
  $dir = get_stylesheet_directory_uri() . '/' . ARTOOLS_DIR . '/deps/acf-pro/';


  // return
  return $dir;

}
add_filter('acf_settings', 'my_acf_settings');

if(!acf_group_exists("RP Tools") || 1==1){
    require_once(get_stylesheet_directory() . '/' . ARTOOLS_DIR . '/deps/acf_rp_fields.php');
}
acf_add_options_sub_page(array(
        'title'=>'Jumpstart',
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
));
define( 'ACF_LITE' , get_artools_option('acf_menu') );

?>