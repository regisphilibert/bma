<?php bma_get_template_part( 'includes/filters', 'post', array('term_slug'=>'realization-type')); ?>
<div class="bma-container" data-no-padding="tablet-desktop"
>
    <div class="bma-loop isotope-go">
        <div class="row">
        <?php query_posts('post_type=realization&posts_per_page=-1') ; ?>
        <?php get_template_part( 'includes/loops/loop-realizations') ?>
        </div>
    </div>
</div>