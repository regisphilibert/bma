<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
<div class="bma-padded-container">
    <div class="item-head">
        <div class="item-title">
            <?php the_title(); ?>
        </div>
        <div class="item-head-bottom">
            <div class="row">
                <div class="col-sm-6 item-date">
                    <?php echo _e('Paru le') ?> <?php bma_the_date(); ?>
                </div>
                <div class="col-sm-6"><?php get_template_part( 'includes/socials' ); ?></div>
            </div>
        </div>
    </div>
    <div class="item-content bma-html">
        <?php the_content(); ?>
    </div>
    <div data-align-with="first-title">
        <?php echo do_shortcode(get_field('bma_job_form', 'options')); ?>
    </div>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>