<?php get_template_part('includes/carousels/carousel', 'home'); ?>
<div class="bma-container home-container">
    <section class="home-main-copy">
        <div class="float-wrapper">
            <div class="home-content item-content bma-html bma-hero-text col-sm-6 col-md-7 col-md-6 col-md-offset-1 ">
                <?php echo get_field('bma_home_content') ?>
            </div>
            <div class="home-aside item-aside bma-html col-sm-6 col-md-5 col-md-4">
                <?php echo get_field('bma_home_aside') ?>
            </div>
        </div>
    </section>
    <div class="whiteblock">
        <!-- RÉALISATIONS -->
        <section class="featured-tax col-sm-12 col-md-10 col-md-offset-1">
            <div class="bma-title with-addon">
                <?php echo _e('Réalisations', 'bma'); ?>
                <div class="bma-addon">
                    <a class="bma-fat-link add-plus" href="<?php echo bloginfo('url') ?>/realisations/"><?php echo _e('Voir nos réalisations', 'bma'); ?></a>
                </div>
            </div>
            <nav class="hero-tax-list" style="overflow:hidden;">
                <?php foreach (get_field('bma_featured_tax') as $k => $item) : $term = get_term( $item, 'realization-type' ); ?>
                <a href="<?php echo get_term_link( $term->term_id, 'realization-type'); ?>" class="hero-tax-list-item">
                    <img class="frame" src="<?php echo get_theme_image('feature-tax-frame.gif'); ?>" alt="<?php echo $term->name; ?>">
                    <div class="sharp" style="background-image:url(<?php echo get_field('bma_tax_image', 'realization-type_'.$term->term_id); ?>)"></div>
                    <div class="blur" style="background-image:url(<?php echo get_field('bma_tax_image_blur', 'realization-type_'.$term->term_id); ?>)"></div>
                    <div class="list-item-content">
                        <div class="valign">
                            <div class="valign-content">
                                <?php echo $term->name; ?>
                            </div>
                        </div>
                    </div>
                </a>
                <?php endforeach; ?>
            </nav>
        </section>
    </div>
</div>