<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
<div class="bma-padded-container">
    <div class="item-head">
        <h1 class="item-title"><?php the_title(); ?></h1>
    </div>
    <div class="item-content bma-html">
        <?php the_content(); ?>
    </div>
    <hr class="bma-layout-hr">
    <?php
        global $persons;
        $persons = get_field('bma_team_members');
        $hero_persons = array(array_shift($persons), array_shift($persons));
    ?>
    <div class="hero-persons row">
    <?php foreach($hero_persons as $post): ?>
    <div class="col-sm-6 hero-person">
        <img class="person-image" src="<?php echo get_field_image('bma_person_image', 'medium') ?>" alt="">
        <div class="hero-person-title">
            <?php the_title(); ?>
        </div>
        <div class="hero-person-desc">
            <?php the_field('bma_person_excerpt'); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="bma-link add-plus"><?php echo _e('En savoir plus'); ?></a>
    </div>
    <?php endforeach; ?>
    </div>
</div>
<div class="bma-padded-container" data-no-padding="desktop"><?php get_template_part( 'includes/loops/loop', 'persons' ); ?></div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>