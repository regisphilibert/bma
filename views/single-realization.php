<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
    <div class="bma-padded-container">
        <div class="item-head">
            <div class="backlink-block"><script>document.write('<a class="backlink" href="' + document.referrer + '">< Retour</a>');</script></div>
            <h1 class="item-title"><?php bma_the_title(); ?></h1>
            <div class="item-location">
                <?php bma_the_location(); ?>
            </div>
        </div>
        <div class="item-main <?php echo bma_has_aside() ? 'has-aside' : '' ?>">
            <div class="item-content bma-html">
                <?php the_content(); ?>

            </div>
            <?php if(bma_has_aside()): ?>
            <div class="item-aside">
                <div class="aside-title">
                    <?php echo _e('En bref', 'bma'); ?>
                </div>
                <div class="aside-content">
                    <?php bma_the_aside(); ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php if($images = get_field('bma_realization_gallery')): ?>
        <div class="bma-gallery row" data-title="<?php bma_the_title(); ?>" data-location="<?php bma_the_location(); ?>">
            <?php $i=1; foreach($images as $image):
                $grid_col = get_field('bma_attachment_grid', $image['id']) ? get_field('bma_attachment_grid', $image['id']) : 12;
            ?>
            <a href="<?php echo $image['url']; ?>" data-index="<?php echo $i; ?>" class="gallery-item col-sm-<?php echo $grid_col; ?>"><img src="<?php echo $image['url']; ?>" alt=""></a>
            <?php $i++; endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>