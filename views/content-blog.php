<?php
    global $template_params;
    $default = array('post_status'=>'publish');
    $cat = $template_params['cat'] ? $template_params['cat'] : false;
    if($cat){
        $default['cat'] = $cat;
    }
    $first_post = get_posts($default);
    $first_post = $first_post[0];
    $post = $first_post;
 ?>
<?php bma_get_template_part( 'includes/filters', 'post'); ?>
<div class="bma-featured">
    <article class="bma-item featured-item">
        <div class="featured-item-image">
            <a href="<?php the_permalink(); ?>"><img src="<?php echo bma_get_thumb_uri('full', true) ?>" alt=""></a>
        </div>
        <div class="featured-item-content">
            <div class="item-category mobile-block">
                <?php bma_the_category(); ?>
            </div>
            <div class="featured-item-title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>
            <div class="featured-item-meta tablet-block desktop-block">
                <?php bma_the_category(); ?><span class="desktop-inline"> | <?php echo _e('Paru le') ?> <?php bma_the_date() ?></span>
            </div>
        </div>
    </article>
</div>
<?php query_posts(array_merge($default, array('posts_per_page'=>get_option('posts_per_page') - 1, 'post__not_in'=>array($first_post->ID)))); ?>
<div class="bma-container" data-no-padding="tablet-desktop">
    <div class="bma-loop" data-is-stacked="mobile" data-count-posts="<?php echo bma_count_posts(); ?>" data-limit="<?php echo get_option('posts_per_page'); ?>">
        <div class="row">
        <?php
            bma_get_template_part( 'includes/loops/loop-posts', false, array('col_class'=>'col-sm-6 col-md-4'));
        ?>
        </div>
        <?php if(!$cat): ?>
        <nav class="bma-more-nav <?php if(bma_count_posts() < get_option('posts_per_page')) { echo 'bma-disabled';} ?>">
            <div class="bma-btn ready <?php if(bma_count_posts() < get_option('posts_per_page')) { echo 'bma-disabled';} ?>" id="ajax_more">
                <?php echo _e("Plus d'articles", 'bma'); ?> <?php echo $cat; ?>
            </div>
        </nav>
        <?php endif; ?>
    </div>
</div>
<?php wp_reset_query(); ?>