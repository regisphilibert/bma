<div class="bma-padded-container error-block">
    <div class="item-head col-sm-8">
        <h1 class="item-title">Page non trouvée!</h1>
        <h2>Erreur 404</h2>
        <p>Nous sommes désolés de cet impair. Nous vous invitons à consulter les autres pages. Merci de votre compréhension et bonne navigation!</p>
        <p><a class="backlink" href="<?php bloginfo('url'); ?>">< Retour à la page d'accueil</a></p>
    </div>
    <div class="item-content bma-html col-sm-4">
        <?php
            wp_nav_menu(array(
                'theme_location'  => 'main-menu',
                'container'       => false,
                'menu_class'      => 'bma-main-menu',
            ));
        ?>
    </div>
</div>