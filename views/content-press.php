<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="bma-padded-container">
    <div class="item-head">
        <div class="item-title">
            <?php the_title(); ?>
        </div>
    </div>
    <div class="item-content bma-html">
        <?php bma_get_layout(); ?>
    </div>
    <section>
        <div class="bma-title">
            <?php echo _e('Presse'); ?>
        </div>
        <div class="bma-press-list bma-column-sort">
            <?php $awards_list = bma_query_awards(); ?>
            <?php foreach($awards_list as $year => $awards): $count = $count + count($awards); ?>
            <div class="bma-group-list" data-count="<?php echo $count; ?>">
                <div class="list-title">
                    <?php echo $year; ?>
                </div>
                <ul>
                    <?php foreach($awards as $award):
                    if($link = get_field('bma_award_file', $award->ID)){
                        $link = $link['url'];
                    }
                    else{
                        $link = get_field('bma_award_url', $award->ID);
                    }
                    $text = get_field('bma_award_text', $award->ID);
                    ?>
                    <li class="list-item">
                        <div class="list-item-title">
                            <?php if($link): ?>
                            <a href="<?php echo $link; ?>" target="_blank">
                            <?php endif; ?>
                            <?php echo $award->post_title; ?>
                            <?php if($link): ?>
                            </a>
                            <?php endif; ?>
                        </div>
                        <div class="list-item-content">
                            <?php echo $text; ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>