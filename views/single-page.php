<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
<div class="bma-padded-container">
    <div class="item-head">
        <h1 class="item-title"><?php the_title(); ?></h1>
    </div>
    <div class="item-content bma-html">
        <?php bma_get_layout(); ?>
    </div>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>