<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="bma-padded-container">
    <div class="item-head">
        <div class="item-title">
            <?php the_title(); ?>
        </div>
    </div>
    <div class="bma-html"><?php bma_get_layout(); ?></div>
    <div class="row">
        <?php $i=1; while(the_repeater_field('bma_options_adresses', 'options')): ?>
        <div class="col-md-6 bma-block bma-address">
            <div class="bma-address-content">
                <div class="block-title"><?php the_sub_field('bma_options_adresses_title', 'options') ?></div>
                <div class="block-content">
                    <?php the_sub_field('bma_options_adresses_text', 'options'); ?>
                </div>
            </div>
            <div class="bma-address-map">
                <div class="map-canvas"
                    id="map-<?php echo $i; ?>"
                    <?php bma_the_latloc_attr(get_sub_field('bma_options_adresses_string', 'options')) ?>
                    data-title="<?php the_sub_field('bma_options_adresses_title') ?>"
                >
                </div>
                <a href="<?php the_sub_field('bma_options_adresses_map_link', 'options'); ?>">
                    <img src="<?php the_sub_field('bma_options_adresses_map_image', 'options'); ?>" />
                </a>
            </div>
        </div>
        <?php $i++; endwhile; ?>
    </div>
    <!-- MapBuilder : https://mapbuildr.com/buildr/kp2b7s -->
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>