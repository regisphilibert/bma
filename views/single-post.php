<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
<div class="bma-padded-container">
    <div class="item-head">
        <h1 class="item-title"><?php bma_the_category(true, 'item-category pull-right'); ?><?php the_title(); ?></h1>
        <div class="item-head-bottom mobile-block tablet-block">
            <div class="row">
                <div class="col-sm-8 item-meta">
                    <span class="tablet-inline"><?php bma_the_author_image(); ?></span> <?php echo _('Par') ?> <?php bma_the_author(true); ?> | <?php echo _e('Publié le') ?> <?php bma_the_date(); ?>
                </div>
                <div class="col-sm-4"><?php get_template_part( 'includes/socials' ); ?></div>
            </div>
        </div>
    </div>
</div>
<?php if($thumb_src = bma_get_thumb_uri('full', false)): ?>
<img src="<?php echo $thumb_src; ?>" alt="" class="item-thumbnail responsive">
<?php endif; ?>
<div class="bma-padded-container">
    <div class="row">
        <div class="col-md-8">
            <div class="item-content bma-html">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item-aside">
                <div class="bma-widget bma-widget-author desktop-block">
                    <div class="widget-image"><?php bma_the_author_image(); ?></div>
                    <div class="widget-content bma-html">
                        <?php echo _('Par') ?> <?php bma_the_author(true); ?><br>
                        <?php echo _e('Publié le') ?> <?php bma_the_date(); ?>
                        <?php get_template_part( 'includes/socials'); ?>
                    </div>
                </div>
                <div class="bma-sidebar">
                    <?php dynamic_sidebar( 'sidebar-main' ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif;
wp_reset_query();
$cat = bma_get_the_category();
if($cat->count > 1):
?>
<div class="bma-padded-container bma-related-posts">
    <div class="bma-loop" data-is-stacked="mobile-tablet-desktop">
        <div class="bma-title">
            <?php echo _e('Aussi à lire', 'bma'); ?>
        </div>
        <?php
            query_posts(
                array(
                    'posts_per_page'=>3,
                    'cat'=>$cat->term_id,
                    'post__not_in'=>array($post->ID)
                )
            );
            bma_get_template_part( 'includes/loops/loop', 'posts');
            wp_reset_query();
        ?>
    </div>
    <?php if($disqus_shortname = get_field('bma_options_disqus_shortname', 'options')): ?>
    <div class="bma-comments">
            <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = '<?php echo $disqus_shortname; ?>'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript><?php echo _e('Veuillez activer javascript pour voir les commentaires', 'bma') ?></noscript>

    </div>
    <?php endif; ?>
</div>
<?php endif; ?>