
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
$author_id = $post->ID;
?>
<!-- post -->
<div class="bma-padded-container">
    <div class="person-head">
        <img class="person-image mobile-block tablet-block" src="<?php echo get_field_image('bma_person_image', 'medium') ?>" alt="">
        <div class="person-head-content">
            <h1 class="item-title"><?php the_title(); ?></h1>
            <hr class="mobile-block tablet-block">
            <div class="person-contact mobile-block tablet-block">
                <?php
                ob_start();
                if(get_field('bma_person_email')): ?>
                    <span class="contact-email"><?php bma_the_email(); ?></span>
                <?php endif; ?>
                <?php if(get_field('bma_person_phone')): ?>
                    <span class="contact-phone"><?php bma_the_phone(); ?></span>
                <?php endif; ?>
                <nav class="contact-social socials">
                   <?php if($facebook = get_field('bma_person_facebook')): ?>
                   <a href="<?php echo $facebook; ?>" class="facebook"></a>
                    <?php endif; ?>
                   <?php if($linkedin = get_field('bma_person_linkedin')): ?>
                   <a href="<?php echo $linkedin; ?>" class="linkedin"></a>
                    <?php endif; ?>
                </nav>
                <?php
                    $person_contact = ob_get_flush();
                ?>
            </div>
        </div>
        <div class="backlink-block"><script>document.write('<a class="backlink" href="' + document.referrer + '">< Retour</a>');</script></div>
    </div>
    <div class="item-main">
        <div class="item-content">
            <?php echo get_field('bma_person_content'); ?>
        </div>
        <div class="item-aside desktop-block">
        <img class="person-image" src="<?php echo get_field_image('bma_person_image', 'medium') ?>" alt="">
        <div class="person-contact">
            <?php echo $person_contact; ?>
        </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php wp_reset_query();
$query_same_author = array(
    'posts_per_page'=>3,
    'meta_key'=>'bma_author',
    'meta_value'=>$author_id
);
if(get_posts($query_same_author)):
?>
<div class="bma-container" data-no-padding="all">
    <section class="bma-post-section">
        <div class="bma-loop" data-is-stacked="mobile-tablet">
            <div class="bma-padded-container">
                <div class="bma-title"><?php echo _e('Articles publiés', 'bma'); ?></div>
            </div>
            <div class="row">
                <?php
                    query_posts($query_same_author);
                    bma_get_template_part( 'includes/loops/loop', 'posts', array('col_class'=>'col-md-4'));
                ?>
            </div>
        </div>
    </section>
</div>
<?php endif; ?>