<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- post -->
<div class="bma-padded-container">
    <div class="item-head">
        <h1 class="item-title"><?php the_title(); ?></h1>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="item-content bma-html">
                <?php the_content(); ?>
            </div>
            <?php $jobs = query_posts( array('post_type'=>'job', 'posts_per_page'=>-1) );?>
            <div class="bma-title title-post">
                <?php echo pluralIt(__('Poste(s) Disponible(s)', 'bma'), __('Poste(s) Disponible(s)', 'bma'), count($jobs)); ?>
            </div>
            <hr>
            <?php
                get_template_part( 'includes/loops/loop', 'jobs' );
                wp_reset_query();
            ?>
        </div>
        <div class="col-md-5">
            <?php echo do_shortcode(get_field('bma_job_form', 'options')); ?>
        </div>
    </div>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>