<header class="bma-header special">
    <div class="bma-container">
        <div class="relative-container">
            <nav class="bma-main-nav">
                <?php
                wp_nav_menu( array(
                    'theme_location'  => 'main-menu',
                    'container'       => false,
                    'menu_class'      => 'bma-main-menu',
                    ) );
                ?>
            </nav>
        </div>
        <div class="bma-hmbgr js-display-nav"><span></span></div>
    </div>
    <div class="bma-container">
        <div class="bma-branding">
            <div class="logo">
                <img src="<?php echo get_theme_image("bma-logo.png") ?>" alt="<?php echo _e('BMA Logo', 'bma' ); ?>">
            </div>
        </div>
    </div>
</header>