<header class="bma-header new">
    <div class="bma-container">
        <div class="relative-container">
            <nav class="bma-main-nav">
                <?php
                wp_nav_menu( array(
                    'theme_location'  => 'main-menu',
                    'container'       => false,
                    'menu_class'      => 'bma-main-menu',
                    ) );
                ?>
            </nav>
        </div>
        <div class="bma-hmbgr js-display-nav"><span></span></div>
    </div>
</header>