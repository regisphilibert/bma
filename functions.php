<?php
require("rptools_core/rptools_functions.php");
require("custom-posts.php");

/*********************************************
            THEME OPTIONS
*********************************************/

define(THEME_NAME, 'BMA'); //Utilisé pour les nom de page d'options et autres.
define(THEME_SHORTNAME, 'bma'); // Utilise comme prefix des tables d'options et autre references code.
define(THEME_ICON , get_stylesheet_directory_uri()."/settings/settings.png");

/*********************************************
            FAVICONS
*********************************************/
function bma_add_favicons() {
    $domain = get_site_url();
    $filename = "favicon";
    if(strpos($domain, '.loc') !== false){
        $filename .= "-local";
    }
    if(strpos($domain, '.ellpi') !== false){
        $filename .= "-staging";
    }
    ?>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri()."/".$filename.".png"; ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png" />
    <meta name="application-name" content="<?php echo bloginfo(('title') ); ?>"/>
    <meta name="msapplication-TileColor" content="#f47721" />
    <meta name="msapplication-TileImage" content="mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
<?php }
add_action('wp_head', 'bma_add_favicons');
add_action( 'admin_head', 'bma_add_favicons' );

function add_class_to_body_class($classes = ""){
    $classes[] = THEME_SHORTNAME;
    if(is_page_template( 'templates/page-blog.php' )){
        $classes[] = 'page-blog';
    }
    if(is_page_template( 'templates/page-contact.php' )){
        $classes[] = 'page-contact';
    }
    if(is_page_template( 'templates/page-press.php' )){
        $classes[] = 'page-press';
    }
    if(is_page_template('templates/page-career.php')){
        $classes[] = 'page-career';
    }
    if(is_tax('realization-type') || is_post_type_archive('realization')){
        $classes[] = 'realization-archive';
    }
    return $classes;
}
add_filter('body_class', 'add_class_to_body_class');
/**
*
* Permet d'afficher une options du theme en renseignant directement son "nom" sans le prefix du theme.
* @param @option : Le nom de l'option
* @return La valeur de l'option en question ou
* - toutes les options du theme si @option n'est pas renseignée ou
* - faux si l'option n'est pas trouvée.
*
*/
function get_theme_option($option = false){
    $options = get_option(THEME_SHORTNAME);
    if($option){
        return $options[THEME_SHORTNAME.'_'.$option] != '' ? $options[THEME_SHORTNAME.'_'.$option] : false ;
    }
    else{
        return empty($options) ? (object)$options : false;
    }
}
//We add the option page of the theme (using ACF);
//@params : http://www.advancedcustomfields.com/resources/acf_add_options_page/
add_rptools_options(array('page_title'=>"Options ".THEME_NAME, 'icon_url'=>'dashicons-admin-settings'));


/*////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
        URL REWRITING
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////*/

add_action( 'wp_loaded','bma_flush_rules' );
add_filter( 'rewrite_rules_array','bma_insert_rewrite_rules' );

function bma_get_rules(){
    return array(
        'realisations/([^/]+)?$'=>'index.php?post_type=realization&realization-type=$matches[1]&term=$matches[1]',
        'realisations/[^/]+/([^/]+)?$'=>'index.php?post_type=realization&name=$matches[1]',
        'blogue/([^/]+)?$'=>'index.php?category_name=$matches[1]',
        'blogue/[^/]+/([^/]+)?$'=>'index.php?post_type=post&name=$matches[1]&page=0',
    );
}
function bma_flush_rules() {
    $rules = get_option( 'rewrite_rules' );
    $rewrite = 0;
    foreach(bma_get_rules() as $regex=>$rules){
        if(!isset($rules[$regex])){
        $rewrite = 1;
        }
        if($rewrite){
            global $wp_rewrite;
            $wp_rewrite->flush_rules();
        }
    }
}
function bma_insert_rewrite_rules( $rules ) {
    $newrules = bma_get_rules();
    return $newrules + $rules;
}

function edit_the_permalink($html){
    global $post;
    $output = $html;
    if(get_post_type() == 'realization'){
        $type = bma_get_the_type();
        $output = get_site_url()."/realisations/".$type->slug."/".$post->post_name;
    }
    elseif(get_post_type() == 'post'){
        $cat = bma_get_the_category();
        $output = get_site_url()."/blogue/".$cat->slug."/".$post->post_name;
    }
    return $output;
}
add_filter('the_permalink', 'edit_the_permalink');

function bma_term_link( $url, $term, $taxonomy ) {
    if($taxonomy == 'realization-type'){
        return get_site_url()."/realisations/".$term->slug;
    }
    elseif($taxonomy == 'category'){
        return get_site_url()."/blogue/".$term->slug;
    }

}
add_filter('term_link', 'bma_term_link', 10, 3);

/*////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
        LE THEME
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////*/

/*********************************************
            LES MENUS
*********************************************/
function register_theme_menus() {
    register_nav_menus(
        array(
            'main-menu' => 'Menu principal',
            'social-menu' => 'Menu Social'
        )
    );
}
add_action( 'init', 'register_theme_menus' );
/*********************************************
            SIDEBARS
*********************************************/
register_sidebar( array(
    'name'      => 'Sidebar Article',
    'id'      => 'sidebar-main',
    'description'  => 'La sidebar qui apparaît sur un article',
    'before_widget' =>  '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => ''
    )
);

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails');
    add_image_size('bma_480', 480, 0 , 0);
    add_image_size('bma_crop_90', 90, 90 , true);
}
/*********************************************
            EXCERPT
*********************************************/
function custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/*********************************************
            ADD TO HEAD
*********************************************/
//Pour ajouter des balise ou des variables javascript dans le head, décommenter pour activer.
add_action( 'wp_head', 'theme_add_to_head' );
function theme_add_to_head(){
    global $post;
    //Pour un site responsive :
    echo '<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">';
    //Des variables JS
    //echo '<script type="text/javascript" src="http://fast.fonts.net/jsapi/44e31a54-a64a-4c6d-8e62-fb80cfa0c9b2.js"></script>';
}
//On register le style nous même (celui à la racine) pour lui ajouter le ver= que l'on veut, Ici c'est un chiffre + . + timestamp de dernière sauvegarde du ficiher.
wp_deregister_style( 'default-style' );
add_action('wp_enqueue_scripts', 'register_theme_styles');
function register_theme_styles(){
    $file_saved = filemtime(get_template_directory()."/style.css");
    wp_enqueue_style( 'default-style', get_template_directory_uri()."/style.css", false, '1.'.$file_saved, $media = 'all' );
}

//Les scripts à ajouter se déclare ici puis s'ajoute dans add_script_to_page() plus bas.
add_action('init', 'register_theme_scripts');
function register_theme_scripts() {
    wp_register_script(
        'head-script',
        get_stylesheet_directory_uri().'/js/head.script-min.js',
        false,
        '1.0',
        false
    );
    wp_register_script(
        'script',
        get_stylesheet_directory_uri().'/js/main.script-min.js',
        false,
        '1.0',
        true
    );
    wp_register_script(
        'google.map',
        "https://maps.googleapis.com/maps/api/js?key=&sensor=false",
        false,
        '1.0',
        false
    );
    wp_register_script(
        'modernizr',
        get_stylesheet_directory_uri().'/js/libs/modernizr.js',
        false,
        '1.0',
        false
    );
}
add_action('wp_enqueue_scripts', 'add_scripts_to_page');
function add_scripts_to_page() {
    wp_enqueue_script( 'head-script' );
    wp_enqueue_script('modernizr');
    wp_enqueue_script( 'script' );
    if(is_page_template('templates/page-contact.php' )){
        wp_enqueue_script('google.map' );
    }
}

/*********************************************
            SHORTCODES
*********************************************/
function bma_btn( $atts, $content = null ) {
  $output="<div class='bma-btn-wrapper'>".do_shortcode( $content )."</div>";
  return $output;
  // do shortcode actions here
}
add_shortcode( 'button','bma_btn' );

function bma_hours($atts, $content = null){
    global $post;
    ob_start();
    ?>
    <dl class="bma-dlist">
        <?php while(the_repeater_field('bma_options_hours', 'options')) : ?>
        <dt><?php the_sub_field('bma_options_hours_title') ?></dt>
        <dd><?php the_sub_field('bma_options_hours_content') ?></dd>
        <?php endwhile; ?>
    </dl>
    <?php
    return ob_get_clean();
}
add_shortcode( 'horaires','bma_hours' );

/*********************************************
            LOAD MORE
*********************************************/
function more_posts(){
  query_posts( "author_name=".$_POST['author_name']."&offset=".$_POST['offset']."&posts_per_page=6&post_status=publish" );
  bma_get_template_part('includes/loops/loop', 'posts', array('col_class'=>'col-sm-6 col-md-4 ajax-pending'));
  wp_reset_query();
  die();
}

if(is_user_logged_in()){
    add_action( 'wp_ajax_more_posts_action', 'more_posts');
}
else{
    add_action( 'wp_ajax_nopriv_more_posts_action', 'more_posts');
}

/*********************************************
            OUR SPECIFICS FUNCTIONS
*********************************************/
function bma_query_awards(){
    $years = get_terms( 'bma-year');
    foreach($years as $year){
        $output[$year->slug] = get_posts(array('bma-year'=>$year->slug, 'post_type'=>'award', 'posts_per_page'=>-1));
    }
    krsort($output);
    return $output;
}

function bma_get_template($type = 'content', $name = false){
    global $post;
    if(!$name){
        $name = get_post_type();
        if(is_archive()){
            $type = "archive";
            $name = get_post_type();
        }
        if(is_single() || is_page()){
            $type = "single";
            $name = get_post_type();
        }
    }
    get_template_part( 'views/'.$type, $name);
}

function bma_get_layout(){
    global $post;
    the_content();
    while(the_flexible_field('bma_layout_rows')){
        switch (get_row_layout()) {
            case 'bma_layout_col1': ?>
                <div class="bma-layout-row col1" <?php echo get_sub_field('bma_layout_col_width') ? "style='max-width:".get_sub_field('bma_layout_col_width')."'" : ""; ?>>
                    <?php the_sub_field('bma_layout_col_1'); ?>
                </div>
            <?php
                break;
            case 'bma_layout_col2': ?>
                <div class="row bma-layout-row col2">
                    <div class="col-sm-6">
                        <?php the_sub_field('bma_layout_col_1') ?>
                    </div>
                    <div class="col-sm-6">
                        <?php the_sub_field('bma_layout_col_2') ?>
                    </div>
                </div>
            <?php
                break;
            case 'bma_layout_col3': ?>
                <div class="row bma-layout-row col3">
                    <div class="col-sm-4">
                        <?php the_sub_field('bma_layout_col_1') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php the_sub_field('bma_layout_col_2') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php the_sub_field('bma_layout_col_3') ?>
                    </div>
                </div>
            <?php
                break;
            case 'bma_layout_sep': ?>
                <hr class="bma-layout-hr">
            <?php
                break;
            default:
                # code...
                break;
        }
    }
}

function has_head_img(){
    global $post;
    if(has_post_thumbnail() && get_post_type() != 'post' && !is_archive()){
        return true;
    }
    return false;
}
function bma_top(){
    get_template_part( 'includes/top' );
}
function bma_bottom(){
    get_template_part( 'includes/bottom' );
}
function bma_container_type(){
    global $post;
    $output = 'default';
    if(is_page_template( 'templates/page-blog.php' )){
        $output = 'mobile';
    }
    if(is_front_page()){
        $output = 'all';
    }
    return $output;
}
function bma_container_class(){
    if(is_front_page()){
        return '';
    }
    return 'bma-container';
}
function bma_body_attr(){
    $attr = array();
    if(is_tax('realization-type')){
        $current_tax = get_query_var('realization-type');
        $attr['data-realization-type'] = $current_tax;
    }
    foreach ($attr as $key => $value) {
        $string .= " $key = '$value'";
    }
    echo $string;
}
function bma_get_current_type(){
        return get_query_var('realization-type', false);
}
function bma_get_template_part($slug, $name = false, $params = false){
    if($params && is_array($params)){
        global $template_params;
        $template_params = $params;
    }
    get_template_part( $slug, $name);
}


function bma_get_thumb_uri($size = false, $placeholder = false){
    global $post;
    $size = $size ? $size : 'bma_480';
    $output = false;
    if(has_post_thumbnail()){
        $output = rp_get_attachment_image_src(get_post_thumbnail_id(), $size);
    }
    elseif($placeholder){
        $output = get_theme_image('bma-placeholder-'.$size.'.jpg');
    }
    return $output;
}

function bma_the_title($short = 0){
    global $post;
    $output = get_the_title();
    if($short && count_chars(get_the_title()) > $short ){
        if($alt_title = get_field('bma_alt_title')){
            $output = $alt_title;
        }
        else{
            $output = shortenIt(get_the_title(), $short, '...');
        }
    }
    echo $output;
}
function bma_the_latloc_attr($string = 0){
    $latlongs = explode(',', $string);
    $lat = $latlongs[0];
    $long = $latlongs[1];
    echo " data-lat='$lat' data-long='$long' ";
}
function bma_the_date($post = false){
    the_time('m/d/y');
}
function bma_the_location($item = false){
    global $post;
    echo get_field('bma_realization_location');
}
function bma_get_location($item_id){
    return get_field('bma_realization_location', $item_id);
}
function bma_has_aside(){
    global $post;
    return get_field('bma_aside');
}
function bma_the_aside(){
    global $post;
    echo get_field('bma_aside');
}
function bma_count_posts($post_type = 'post'){
   $output = wp_count_posts($post_type);
   return $output->publish;

}

/*********************************************
TO BE ADRESSED
*********************************************/
function bma_the_category($link = true, $class = ''){
    global $post;
    $cat = current(get_the_category( $post->ID ));
    if($link){
        echo "<a class='$class' href='".get_category_link($cat->term_id)."'>".$cat->name."</a>";
    }
    else{
        echo $cat->name;
    }
}
function bma_get_the_type($post_id = false){
    if($post_id){
        $post = get_post($post_id);
    }
    else{
        global $post;
    }
    $type = current(get_the_terms($post->ID, 'realization-type'));
    return $type;
}
function bma_get_the_category(){
    if($post_id){
        $post = get_post($post_id);
    }
    else{
        global $post;
    }
    $cat = current(get_the_category( $post->ID ));
    return $cat;
}
function bma_the_author($link = false){
    global $post;
    $href = false;
    if($author_id = get_field('bma_author')){
        $author = get_the_title( $author_id );
        $href = get_permalink( $author_id);
    }
    else{
        $author = get_the_author();
    }
    if($link && $href){
        echo "<a href='$href'>$author</a>";
    }
    else{
        echo $author;
    }
}

function bma_get_the_person_image($post_id = false, $size = false){
    global $post;
    $post_id = !$post_id ? $post->ID : $post_id;
    $size = $size ? $size : 'thumbnail';
    $image = get_field_image('bma_person_image', $size, $post_id);
    if(!$image){
        $image = get_theme_image('mystery-person.jpg');
    }
    return $image;
}
function bma_the_person_image($size = false){
    global $post;
    $size = $size ? $size : 'medium';
    if($image = bma_get_the_person_image($post->ID, $size)){
        echo $image;
    }
}
function bma_the_author_image($size = false){
    global $post;
    if($author_id = get_field('bma_author', $post->ID)){
        $image = bma_get_the_person_image($author_id, $size);
    }
    else{
        $image = get_theme_image('mystery-person.jpg');
    }
    echo "<img src='".$image."' class='person-image bma-generated-image' />";
}
function bma_the_email(){
    global $post;
    echo rot13_email(get_field('bma_person_email'));
}
function bma_the_phone(){
    global $post;
    echo get_field('bma_person_phone');
}
function bma_the_categories($term_slug = false){
    global $post;
    global $wp_query;
    $term_slug = $term_slug ? $term_slug : 'category';
    $cats = get_terms($term_slug, array('number'=>4));
    $realization_type = get_query_var('realization-type', false);
    ?>
    <a data-filter="*" href="<?php echo bloginfo('url') ?>/realisations" class="<?php echo !$realization_type ? 'active' : '' ?>"><?php echo _e('Tous', 'bma') ?></a>
    <?php foreach($cats  as $cat): ?>
        <a class="<?php echo $realization_type == $cat->slug ? 'active' : '';?>" data-filter=".<?php echo $cat->slug; ?>" data-type="<?php echo $cat->slug; ?>" href="<?php echo get_term_link( $cat, 'category' ); ?>"><?php echo $cat->name; ?></a>
    <?php endforeach;
}

/*********************************************
            WIDGETS
*********************************************/
/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
 */
class BMA_About_Widget extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     **/
    function BMA_About_Widget() {
        $widget_ops = array( 'classname' => 'bma-about bma-widget', 'description' => "Le widget À Propos de BMA" );
        $this->WP_Widget( 'bma-widget-about', 'BMA - À Propos', $widget_ops );
    }
    function form( $instance ) {
        $instance = $instance;
    }
    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        echo $before_widget;
        ?>
        <div class="bma-widget-title">
            <?php echo _e('À Propos de BMA', 'bma'); ?>
        </div>
        <div class="widget-content bma-html">
            <?php echo get_field('bma_widget_about_content', 'widget_' . $widget_id); ?>
        </div>
        <?php
        echo $after_widget;
    }
}

add_action( 'widgets_init', create_function( '', "register_widget( 'BMA_About_Widget' );" ) );

/*********************************************
            WP MIGRATE PRO
*********************************************/
add_filter('wpmdb_preserved_options', function($preserved_options) {

    $preserved_options = array_merge($preserved_options, array(
        'bma_options_disqus_shortname',
    ));
    return array_unique($preserved_options);
});
?>