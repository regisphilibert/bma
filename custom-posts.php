<?php
/*********************************************
            CUSTOM POSTS
*********************************************/
add_action( 'init', 'ap_create_post_type' );
function ap_create_post_type() {
    $post_types = array(
        "realization" => array(
                'labels' => array(
                    'name' => __( 'Réalisations', 'bma' ),
                    'singular_name' => __( 'Réalisation', 'bma' ),
                    'add_new'=>'Ajouter',
                    'add_new_item'=>'Ajouter une Réalisation',
                    'edit_item'=> 'Modifier la Réalisation'
                ),
            'public' => true,
            'menu_icon' => 'dashicons-building',
            'has_archive' => true,
            'menu_position' => 5,
            'rewrite' => array('slug' => 'realisations', 'with_front' => '1'),
            'supports'=>array('editor', 'title', 'thumbnail')
        ),
        "person" => array(
                'labels' => array(
                    'name' => __( 'Membres', 'bma' ),
                    'singular_name' => __( 'Membre', 'bma' ),
                    'add_new'=>'Ajouter',
                    'add_new_item'=>'Ajouter un membre',
                    'edit_item'=> 'Modifier la membre'
                ),
            'public' => true,
            'menu_icon' => 'dashicons-groups',
            'has_archive' => false,
            'menu_position' => 5,
            'show_in_nav_menus' => false,
            'rewrite' => array('slug' => 'la-firme/notre-equipe', 'with_front' => '1'),
            'supports'=>array('title')
        ),
        "award" => array(
                'labels' => array(
                    'name' => __( 'Presses', 'bma' ),
                    'singular_name' => __( 'Presse', 'bma' ),
                    'add_new'=>'Ajouter',
                    'add_new_item'=>'Ajouter une Presse',
                    'edit_item'=> 'Modifier la Presse'
                ),
            'public' => true,
            'menu_icon' => 'dashicons-megaphone',
            'has_archive' => false,
            'menu_position' => 5,
            'show_in_nav_menus' => false,
            'rewrite' => array('slug' => 'prix', 'with_front' => '1'),
            'supports'=>array('title')
        ),
        "job" => array(
                'labels' => array(
                    'name' => __( 'Emplois', 'bma' ),
                    'singular_name' => __( 'Emploi', 'bma' ),
                    'add_new'=>'Ajouter',
                    'add_new_item'=>"Ajouter une offre d'Emploi",
                    'edit_item'=> "Modifier l'offre"
                ),
            'public' => true,
            'menu_icon' => 'dashicons-nametag',
            'has_archive' => false,
            'menu_position' => 5,
            'show_in_nav_menus' => false,
            'rewrite' => array('slug' => 'nous-joindre/carriere', 'with_front' => '1'),
            'supports'=>array('title', 'editor')
        ),
    );

    foreach($post_types as $slug => $args){
        register_post_type( $slug, $args );
    }
}

register_taxonomy(
    'realization-type',
    array('realization'),
    array(
        'hierarchical' => true,
        'label' => 'Types de Réalisations',
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'type-de-realisations'),
        'singular_label' => 'Type de Réalisations'
    )
);
register_taxonomy(
    'bma-year',
    array('award'),
    array(
        'hierarchical' => false,
        'label' => 'Années',
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'query_var' => true,
        'singular_label' => 'Année'
    )
);
?>