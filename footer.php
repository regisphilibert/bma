<div class="bma-footer-container">
    <footer class="bma-footer bma-padded-container">
        <div class="row">
            <div class="col-sm-4 col-md-3 col-md-offset-1 bma-block">
                <div class="block-title hero-email"><?php echo rot13_email(get_field('bma_options_email', 'options')); ?></div>
                <hr>
                <nav class="footer-socials">
                    <?php while(the_repeater_field('bma_options_socials', 'options')): ?>
                    <a target="_blank" href="<?php the_sub_field('bma_options_social_url') ?>"><img src="<?php the_sub_field('bma_options_social_image') ?>" alt="<?php the_sub_field('bma_options_social_name') ?>"></a>
                    <?php endwhile; ?>
                </nav>
            </div>
            <?php while(the_repeater_field('bma_options_adresses', 'options')): ?>
            <div class="col-sm-4 col-md-3 col-md-push-1 bma-block bma-address">
                <div class="block-title"><?php the_sub_field('bma_options_adresses_title', 'options'); ?></div>
                <div class="block-content">
                    <?php the_sub_field('bma_options_adresses_text', 'options'); ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </footer>
    <div class="bma-copyrights">
        Copyright © <?php the_date('Y'); ?> Bourassa Maillé architectes<span class="sep">|</span><a class="ellpi" href="http://www.ellpi.com" target="_blank">Site web par Ellpi</a>
    </div>
</div>